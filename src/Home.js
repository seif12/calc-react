import React from 'react';
import './Home.css';

class Home extends React.Component{
    constructor()
    {
        super();
        this.state = { input: ''}
        this.numberClick = this.numberClick.bind(this);
    }
    numberClick(e){
        if(e.target.value === '=')
        {
            this.setState({input: eval(this.state.input)});
        }
        else if(e.target.value === 'Del')
        {
            this.setState({input: ' '});
        }
        else{
        this.setState({input: this.state.input +  e.target.value })}
    }
    
    render(){
    return (
    <div id="background">
        <div id="result">{this.state.input}</div>
        <div id="main">
        
            <div id="first-rows">
                <button onClick={this.numberClick} id="delete" value="Del">Del</button>
                <button onClick={this.numberClick} value="%" class="btn-style operator opera-bg">%</button>
                <button onClick={this.numberClick} value="+" class="btn-style opera-bg value operator">+</button>
            </div>

            <div class="rows">
                <button value="7" class="btn-style  num first-child">7</button>
                <button onClick={this.numberClick} value="8" class="btn-style num">8</button>
                <button onClick={this.numberClick} value="9" class="btn-style num">9</button>
                <button onClick={this.numberClick}  value="-" class="btn-style opera-bg operator">-</button>
            </div>

                <div class="rows">
                    <button onClick={this.numberClick} value="4" class="btn-style num first-child">4</button>
                    <button onClick={this.numberClick} value="5" class="btn-style num">5</button>
                    <button onClick={this.numberClick} value="6" class="btn-style num">6</button>
                    <button onClick={this.numberClick} value="*" class="btn-style opera-bg operator">x</button>
                </div>

                <div class="rows">
                    <button onClick={this.numberClick} value="1" class="btn-style num first-child">1</button>
                    <button onClick={this.numberClick} value="2" class="btn-style num">2</button>
                    <button onClick={this.numberClick} value="3" class="btn-style num">3</button>
                    <button onClick={this.numberClick} value="/" class="btn-style opera-bg operator">/</button>
                </div>

                <div class="rows">
                    <button onClick={this.numberClick} value="0" class="zero" id="delete">0</button>
                    <button onClick={this.numberClick} value="." class="btn-style period opera-bg ">.</button>
                    <button onClick={this.numberClick} value="=" class="eqn">=</button>
                </div>
            </div>
        </div>
         );
    }
}

export default Home;